import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {
  operator: any = '';
  subText: any = '';
  mainText: any = '';
  operatorSet: boolean = false;
  operand1: number;
  operand2: number;
  calculationString = '';
  answered: boolean = false;

  constructor() {  }

  ngOnInit() {
  }

  clickFunction(param) {
    if (param === 'allClear') {
      this.subText = '';
      this.mainText = '';
      this.answered = false;
      this.operatorSet = false;
    } else if (param === 'percent') {
      this.operand1 = parseFloat(this.mainText);
      this.mainText = this.operand1 / 100;
    } else if (param === 'plusMinus') {

    }
  }

  clickKey(key) {
    if (key === '/' || key === 'x' || key === '-' || key === '+') {
      const lastKey = this.mainText[this.mainText.length - 1];
      if (lastKey === '/' || lastKey === 'x' || lastKey === '-' || lastKey === '+')  {
        this.operatorSet = true;
      }
      if ((this.operatorSet) || (this.mainText === '')) {
        return;
      }
      this.operand1 = parseFloat(this.mainText);
      this.operator = key;
      this.operatorSet = true;
    }
    if (this.mainText.length === 11) {
      return;
    }
    this.mainText += key;
  }

  getAnswer() {
    this.calculationString = this.mainText;
    this.operand2 = parseFloat(this.mainText.split(this.operator)[1]);
    if (this.operator === '/') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 / this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = this.mainText.substr(0, 9);
      }
    } else if (this.operator === 'x') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 * this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = 'Error!';
        this.subText = 'Range Melebihi Batas';
      }
    } else if (this.operator === '-') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 - this.operand2).toString();
      this.subText = this.calculationString;
    } else if (this.operator === '+') {
      this.subText = this.mainText;
      this.mainText = (this.operand1 + this.operand2).toString();
      this.subText = this.calculationString;
      if (this.mainText.length > 9) {
        this.mainText = 'Error!';
        this.subText = 'Range Melebihi Batas';
      }
    } else {
      this.subText = 'Error!';
    }
    this.answered = true;
  }
}
